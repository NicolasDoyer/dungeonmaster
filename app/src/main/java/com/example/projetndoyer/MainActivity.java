package com.example.projetndoyer;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projetndoyer.entity.Game;
import com.example.projetndoyer.entity.GameLeaderboard;
import com.example.projetndoyer.entity.GameScore;
import com.example.projetndoyer.entity.GameSettings;
import com.example.projetndoyer.entity.combat.Combat;
import com.example.projetndoyer.entity.dungeon.Cell;
import com.example.projetndoyer.entity.dungeon.helper.CellBackground;

import java.util.Calendar;

/**
 * Main activity
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Game
     */
    private Game game;

    /**
     * Leaderboard instance to store score
     */
    private GameLeaderboard gameLeaderboard;

    /**
     * Cell's layout
     */
    private TableLayout tableLayout;

    /**
     * Player's hp bar
     */
    private ProgressBar hpBar;

    /**
     * Unexplored cells text
     */
    private TextView unexploredCells;

    /**
     * Player's power text
     */
    private TextView playerPower;

    /**
     * Event title text
     */
    private TextView eventTitle;

    /**
     * Event description text
     */
    private TextView eventDescription;

    /**
     * Current level text
     */
    private TextView level;

    /**
     * Cell's onclick event
     */
    private View.OnClickListener onCellClick = this::handleCellClick;

    /**
     * Last clicked cell
     */
    private Button lastClickedCell;

    /**
     * Combat event id
     */
    public static final int LAUNCH_CELL_COMBAT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tableLayout = findViewById(R.id.game_cells);
        hpBar = findViewById(R.id.player_hp);
        unexploredCells = findViewById(R.id.unexplored_cells);
        playerPower = findViewById(R.id.player_power);
        eventTitle = findViewById(R.id.game_event_title);
        eventDescription = findViewById(R.id.game_event_description);
        level = findViewById(R.id.level);
        initializeGame();
    }

    /**
     * Initialize game entity and add button to grid layout
     */
    private void initializeGame() {
        gameLeaderboard = new GameLeaderboard(this);
        game = new Game();
        buildDungeonUI();
    }

    /**
     * Build table layout with all game cells
     */
    private void buildDungeonUI() {
        TableRow currentTableRow = null;
        int columnCounter = 0;
        int cellCounter = 1;

        // Create table row each 'Dungeon size' element
        for (Cell cell : game.getDungeon().getCells()) {
            Button newButton = new Button(this);

            if (columnCounter == 0) {
                currentTableRow = new TableRow(this);
                tableLayout.addView(currentTableRow);
                columnCounter = GameSettings.dungeonSize;
            }

            // Add the new button to the layout + add click event and add data
            buildButtonLayout(newButton);
            CellBackground cellBackground = CellBackground.getBackground(cellCounter, GameSettings.dungeonSize);
            newButton.setTag(R.id.button_cell_position, cellCounter);
            newButton.setTag(R.id.button_cell_background, cellBackground);
            newButton.setBackgroundResource(cellBackground.getFoggyBackground());
            addButtonEvent(newButton, cell);
            currentTableRow.addView(newButton);
            columnCounter--;
            cellCounter++;
        }

        // Initialize bottom UI
        hpBar.setMax(GameSettings.maxPlayerHP);
        updateBottomUI(0);
    }

    /**
     * Build button width and height
     *
     * @param newButton: button to apply new layout
     */
    private void buildButtonLayout(Button newButton) {
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64, getResources().getDisplayMetrics());
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64, getResources().getDisplayMetrics());
        TableRow.LayoutParams layout = new TableRow.LayoutParams(width, height);
        layout.setMargins(1, 1, 1, 1);
        newButton.setLayoutParams(layout);
    }

    /**
     * Clear all cells
     */
    private void clearDungeonUI() {
        tableLayout.removeAllViews();
    }

    /**
     * Add event between the button and a cell
     *
     * @param button: Button
     * @param cell:   Cell
     */
    private void addButtonEvent(Button button, Cell cell) {
        button.setTag(R.id.button_cell_tag, cell);
        button.setOnClickListener(onCellClick);
    }

    /**
     * Handle cell click
     *
     * @param view: clicked button
     */
    private void handleCellClick(View view) {
        Cell attachedCell = (Cell) view.getTag(R.id.button_cell_tag);
        lastClickedCell = (Button) view;
        if (game.isFinished()) {
            Toast.makeText(this, R.string.game_already_finished,
                    Toast.LENGTH_LONG).show();
        } else if (attachedCell.getExplored()) {
            Toast.makeText(this, R.string.cell_already_explored,
                    Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(this, CombatActivity.class);
            intent.putExtra(CombatActivity.GAME_EXTRA, game);
            intent.putExtra(CombatActivity.CLICKED_CELL, attachedCell);
            startActivityForResult(intent, LAUNCH_CELL_COMBAT);
        }
    }

    /**
     * Update bottom UI with current player stats
     */
    private void updateBottomUI(int eventDescription) {
        hpBar.setProgress(game.getPlayer().getHp());
        playerPower.setText(String.valueOf(game.getPlayer().getPower()));
        unexploredCells.setText(String.valueOf(game.getDungeon().getRemainingUnexploredCells()));
        level.setText(String.valueOf(game.getCurrentLevel()));
        if (eventDescription != 0) {
            this.eventTitle.setText(R.string.combat_result_title);
            this.eventDescription.setText(eventDescription);
        }
    }

    /**
     * Check game status
     */
    private void checkGameStatus() {
        if (game.isFinished()) {
            if (game.isVictory()) {
                eventTitle.setText(R.string.game_victory);
                eventDescription.setText(R.string.game_victory_description);
                clearDungeonUI();
                game.nextLevel();
                buildDungeonUI();
            } else {
                eventTitle.setText(R.string.game_defeat);
                eventDescription.setText(R.string.restart_game);
                showScoreDialog();
            }
        }
    }

    /**
     * Score dialog when game is finished
     */
    private void showScoreDialog() {
        EditText playerName = new EditText(this);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Save your score !")
                .setMessage("Enter your username below:")
                .setView(playerName)
                .setPositiveButton("Save", (dialog1, which) -> {
                    GameScore newScore = new GameScore();
                    newScore.setPlayerName(playerName.getText().toString());
                    newScore.setPlayerLevel(game.getCurrentLevel());
                    newScore.setPlayerPower(game.getPlayer().getPower());
                    newScore.setDate(Calendar.getInstance().getTime());
                    gameLeaderboard.addScore(newScore);
                    gameLeaderboard.saveScores(this);
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    /**
     * Settings dialog
     */
    private void showSettingsDialog() {

        int layoutResource = R.layout.settings_dialog;
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(layoutResource, null);

        EditText maxPlayerHp = dialogLayout.findViewById(R.id.max_player_hp);
        EditText initialPower = dialogLayout.findViewById(R.id.initial_player_power);
        EditText minHostilePower = dialogLayout.findViewById(R.id.minimal_hostile_power);
        EditText maxHostilePower = dialogLayout.findViewById(R.id.maximal_hostile_power);
        EditText dungeonSize = dialogLayout.findViewById(R.id.dungeon_size);

        maxPlayerHp.setText(String.valueOf(GameSettings.maxPlayerHP));
        initialPower.setText(String.valueOf(GameSettings.initialPlayerPower));
        minHostilePower.setText(String.valueOf(GameSettings.initialMinHostilePower));
        maxHostilePower.setText(String.valueOf(GameSettings.initialMaxHostilePower));
        dungeonSize.setText(String.valueOf(GameSettings.dungeonSize));

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Game settings")
                .setView(dialogLayout)
                .setPositiveButton("Save", (dialog1, which) -> {
                    int newMaxHp = Integer.valueOf(maxPlayerHp.getText().toString());
                    int newInitialPower = Integer.valueOf(initialPower.getText().toString());
                    int newMinHostilePower = Integer.valueOf(minHostilePower.getText().toString());
                    int newMaxHostilePower = Integer.valueOf(maxHostilePower.getText().toString());
                    int newDungeonSize = Integer.valueOf(dungeonSize.getText().toString());

                    // Check all data
                    if (newMaxHp > 0) {
                        GameSettings.maxPlayerHP = newMaxHp;
                    }
                    if (newInitialPower > 0) {
                        GameSettings.initialPlayerPower = newInitialPower;
                    }
                    if (newMinHostilePower > 0 && newMaxHostilePower > 0 && (newMaxHostilePower - newMinHostilePower >= 1)) {
                        GameSettings.initialMinHostilePower = newMinHostilePower;
                        GameSettings.initialMaxHostilePower = newMaxHostilePower;
                    }
                    if (newDungeonSize >= 2 && newDungeonSize < 6) {
                        GameSettings.dungeonSize = newDungeonSize;
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_CELL_COMBAT && data != null) {
            // Recover button data
            Cell attachedCell = (Cell) lastClickedCell.getTag(R.id.button_cell_tag);
            CellBackground cellBackground = (CellBackground) lastClickedCell.getTag(R.id.button_cell_background);

            switch (data.getIntExtra(CombatActivity.COMBAT_RESULT, 0)) {
                case Combat.WIN_VALUE:
                    attachedCell.setExplored(true);
                    lastClickedCell.setBackgroundResource(cellBackground.getExploredBackground());
                    if (attachedCell.getItem() != null)
                        attachedCell.getItem().use(game.getPlayer()); // Use item if item found
                    game.onPlayerWin();
                    updateBottomUI(R.string.combat_win);
                    break;
                case Combat.LOSE_VALUE:
                    game.onPlayerLoose();
                    lastClickedCell.setBackgroundResource(cellBackground.getNormalBackground());
                    updateBottomUI(R.string.combat_lose);
                    break;
                default:
                    game.onPlayerEscape();
                    lastClickedCell.setBackgroundResource(cellBackground.getNormalBackground());
                    updateBottomUI(R.string.combat_escaped);
                    break;
            }
            this.checkGameStatus();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.restart_option:
                clearDungeonUI();
                initializeGame();
                return true;
            case R.id.leave_option:
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                return true;
            case R.id.settings:
                showSettingsDialog();
                return true;
            case R.id.leaderboard:
                Intent intent = new Intent(this, LeaderboardActivity.class);
                startActivity(intent);
            default:
                return false;
        }
    }
}
