package com.example.projetndoyer.entity;

/**
 * Global game settings class
 */
public class GameSettings {

    /**
     * Player max hp setting
     */
    public static int maxPlayerHP = 10;

    /**
     * Player initial power setting
     */
    public static int initialPlayerPower = 100;

    /**
     * Hostile default max power
     */
    public static int initialMaxHostilePower = 150;

    /**
     * Hostile default minimum power
     */
    public static int initialMinHostilePower = 1;

    /**
     * Default Dungeon size
     */
    public static int dungeonSize = 4;

}
