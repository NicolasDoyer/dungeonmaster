package com.example.projetndoyer.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Represent a score
 */
public class GameScore implements Serializable {

    /**
     * Player's name
     */
    private String playerName;

    /**
     * Level achieved
     */
    private int playerLevel;

    /**
     * Player's power
     */
    private int playerPower;

    /**
     * Score date
     */
    private Date date;

    /**
     * Default GameScore constructor
     */
    public GameScore() {

    }

    /**
     * Get player's name
     *
     * @return player's name
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Set player's name
     *
     * @param playerName: player's name to set
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * Get player's level
     *
     * @return player's level
     */
    public int getPlayerLevel() {
        return playerLevel;
    }

    /**
     * Set player's level
     *
     * @param playerLevel: player's level to set
     */
    public void setPlayerLevel(int playerLevel) {
        this.playerLevel = playerLevel;
    }

    /**
     * Get player's power
     *
     * @return player's power
     */
    public int getPlayerPower() {
        return playerPower;
    }

    /**
     * Set player's power
     *
     * @param playerPower: player's power to set
     */
    public void setPlayerPower(int playerPower) {
        this.playerPower = playerPower;
    }

    /**
     * Get score date
     *
     * @return score date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set score date
     *
     * @param date: score date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
}
