package com.example.projetndoyer.entity.character.helper;

import com.example.projetndoyer.entity.character.DarkAngel;
import com.example.projetndoyer.entity.character.Demon;
import com.example.projetndoyer.entity.character.Ectoplasm;
import com.example.projetndoyer.entity.character.FallenKing;
import com.example.projetndoyer.entity.character.Hostile;

import java.util.Random;

/**
 * Class HostileFactory used to build hostile
 */
public class HostileFactory implements IHostileFactory {

    /**
     * Random hostile power type generation
     */
    public static final int RANDOM_POWER = 1;

    /**
     * Get a hostile with specific type
     *
     * @param type:         Type of the hostile
     * @param minimalPower: Minimal hostile power
     * @param maximalPower: Maximal hostile power
     * @return Hostile
     */
    @Override
    public Hostile getHostile(int type, int minimalPower, int maximalPower) {
        if (type == RANDOM_POWER) {
            // Generate random power
            Random r = new Random();
            int randomPower = r.nextInt(maximalPower - minimalPower) + minimalPower;
            return getRandomType(randomPower);
        }
        return new Demon();
    }

    /**
     * Generate a random Hostile subclass
     *
     * @param power: hostile's power
     * @return Hostile
     */
    private Hostile getRandomType(int power) {
        Random r = new Random();
        int randomType = r.nextInt(4);
        switch (randomType) {
            case 0:
                return new DarkAngel(power);
            case 1:
                return new Ectoplasm(power);
            case 2:
                return new FallenKing(power);
            default:
                return new Demon(power);
        }
    }
}
