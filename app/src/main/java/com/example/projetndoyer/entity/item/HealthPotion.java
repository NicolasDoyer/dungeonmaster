package com.example.projetndoyer.entity.item;

import android.graphics.Color;

import com.example.projetndoyer.R;
import com.example.projetndoyer.entity.character.Player;

import java.util.Random;

public class HealthPotion extends AbstractItem {

    private int healthRegeneration;

    public HealthPotion() {
        healthRegeneration = 1 + new Random().nextInt(3);
        imageResource = R.drawable.health_potion;
        description = "+ " + healthRegeneration + " HP";
        descriptionColor = Color.GREEN;
    }

    @Override
    public void use(Player player) {
        player.setHp(player.getHp() + healthRegeneration);
    }

}
