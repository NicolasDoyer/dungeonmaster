package com.example.projetndoyer.entity.character;

import com.example.projetndoyer.R;

/**
 * Demon class
 */
public class Demon extends Hostile {

    /**
     * Default Demon constructor
     */
    public Demon() {
        name = "Demon";
    }

    /**
     * Power Demon constructor
     *
     * @param power: Demon's power
     */
    public Demon(int power) {
        super(power);
        imageResource = R.drawable.demon;
        name = "Demon";
    }
}
