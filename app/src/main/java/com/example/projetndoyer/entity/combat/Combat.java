package com.example.projetndoyer.entity.combat;

import com.example.projetndoyer.entity.character.Hostile;
import com.example.projetndoyer.entity.character.Player;

/**
 * Class that handles combat player vs hostile
 */
public class Combat {

    /**
     * Global lose status value
     */
    public static final int LOSE_VALUE = 0;

    /**
     * Global win status value
     */
    public static final int WIN_VALUE = 1;

    /**
     * Global escape status value
     */
    public static final int ESCAPE_VALUE = -1;

    /**
     * Launch a Combat
     *
     * @param player:  Player
     * @param hostile: Enemy
     * @return true if player win
     */
    public static boolean launchCombat(Player player, Hostile hostile) {
        return (player.getPower() * Math.random()) - (hostile.getPower() * Math.random()) >= 0;
    }
}
