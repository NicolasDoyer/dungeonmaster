package com.example.projetndoyer.entity.item;

import com.example.projetndoyer.entity.character.Player;

import java.io.Serializable;

public interface Usable extends Serializable {
    public void use(Player player);
}
