package com.example.projetndoyer.entity.character;

import java.io.Serializable;

/**
 * Represents a Character
 */
public abstract class Character implements Serializable {

    /**
     * Character's power
     */
    int power;

    /**
     * Get Character's power
     *
     * @return character's power
     */
    public int getPower() {
        return power;
    }

    /**
     * Set character's power
     *
     * @param power: power to set
     */
    public void setPower(int power) {
        this.power = power;
    }
}
