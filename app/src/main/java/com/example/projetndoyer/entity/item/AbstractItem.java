package com.example.projetndoyer.entity.item;

import android.graphics.Color;

public abstract class AbstractItem implements Usable {

    int imageResource = 0;
    int descriptionColor = Color.BLACK;
    String description;

    public int getImageResource() {
        return imageResource;
    }

    public int getDescriptionColor() {
        return descriptionColor;
    }

    public String getDescription() {
        return description;
    }
}
