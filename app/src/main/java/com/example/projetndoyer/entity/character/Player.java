package com.example.projetndoyer.entity.character;

/**
 * Represents a player
 */
public class Player extends Character {

    /**
     * Player current hp
     */
    private int hp;

    /**
     * Player max hp
     */
    private int maxHp;

    /**
     * Player default constructor
     *
     * @param power: Player's power
     * @param maxHp: Player's max hp
     */
    public Player(int power, int maxHp) {
        this.power = power;
        this.hp = maxHp;
        this.maxHp = maxHp;
    }

    /**
     * Get Player's power
     *
     * @return Player's power
     */
    public int getPower() {
        return power;
    }

    /**
     * Set Player's power
     *
     * @param power: power to set
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * Get current Player's hp
     *
     * @return current Player's hp
     */
    public int getHp() {
        return hp;
    }

    /**
     * Set current Player's hp
     *
     * @param hp: hp tp set
     */
    public void setHp(int hp) {
        if (hp < 0) {
            this.hp = 0;
        } else if (hp > maxHp) {
            this.hp = maxHp;
        } else {
            this.hp = hp;
        }
    }

    /**
     * Get Player's max hp
     *
     * @return Player's max hp
     */
    public int getMaxHp() {
        return maxHp;
    }

    /**
     * Check if player is dead
     *
     * @return player status
     */
    public boolean isDead() {
        return this.hp <= 0;
    }
}
