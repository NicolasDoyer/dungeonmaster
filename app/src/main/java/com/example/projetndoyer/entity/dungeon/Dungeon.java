package com.example.projetndoyer.entity.dungeon;

import com.example.projetndoyer.entity.item.HealthPotion;
import com.example.projetndoyer.entity.item.PowerCharm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * Represents a dungeon
 */
public class Dungeon implements Serializable {

    /**
     * Dungeon's cells
     */
    private ArrayList<Cell> cells;

    /**
     * Dungeon size
     */
    private int dungeonSize;

    /**
     * Default Dungeon constructor
     *
     * @param minimalHostilePower: Minimal hostile's power in cell
     * @param maximalHostilePower: Maximal hostile's power in cell
     * @param dungeonSize:         Dungeon size
     */
    public Dungeon(int minimalHostilePower, int maximalHostilePower, int dungeonSize) {
        this.cells = new ArrayList<>();
        this.dungeonSize = dungeonSize;
        for (int i = 0; i < dungeonSize * dungeonSize; i++) { // Init cells
            cells.add(new Cell(minimalHostilePower, maximalHostilePower));
        }
        generateLoot();
    }

    /**
     * Get Dungeon's cells
     *
     * @return Dungeon's cells
     */
    public ArrayList<Cell> getCells() {
        return cells;
    }

    /**
     * Retrieve how many cells still unexplored
     *
     * @return unexplored cells count
     */
    public int getRemainingUnexploredCells() {
        int unexploredCell = 0;
        for (Cell cell : cells) {
            if (!cell.getExplored())
                unexploredCell++;
        }
        return unexploredCell;
    }

    /**
     * Generate randomly loot
     */
    private void generateLoot() {
        Random random = new Random();
        HealthPotion potion = new HealthPotion();
        PowerCharm powerCharm = new PowerCharm();

        int randomIndex = random.nextInt(cells.size());
        int secondRandomIndex = random.nextInt(cells.size());
        if (secondRandomIndex == randomIndex) {
            secondRandomIndex = (secondRandomIndex + 1) % cells.size();
        }

        cells.get(randomIndex).setItem(potion);
        cells.get(secondRandomIndex).setItem(powerCharm);
    }

    /**
     * Get Dungeon size
     *
     * @return Dungeon size
     */
    public int getDungeonSize() {
        return dungeonSize;
    }
}
