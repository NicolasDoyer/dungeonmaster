package com.example.projetndoyer.entity.dungeon.helper;

import com.example.projetndoyer.R;

import java.io.Serializable;
import java.util.Random;

/**
 * Cell background Creator
 */
public enum CellBackground implements Serializable {

    BottomBackground(R.drawable.bottom_floor, R.drawable.foggy_bottom_floor, R.drawable.beaten_bottom_floor),
    BottomLeftBackground(R.drawable.bottom_floor_left_corner, R.drawable.foggy_bottom_floor_left_corner, R.drawable.beaten_bottom_floor_left_corner),
    BottomRightBackground(R.drawable.bottom_floor_right_corner, R.drawable.foggy_bottom_floor_right_corner, R.drawable.beaten_bottom_floor_right_corner),
    LeftBackground(R.drawable.left_floor, R.drawable.foggy_left_floor, R.drawable.beaten_left_floor),
    RightBackground(R.drawable.right_floor, R.drawable.foggy_right_floor, R.drawable.beaten_right_floor),
    NormalBackground(R.drawable.floor, R.drawable.foggy_floor, R.drawable.beaten_floor),
    NormalBackground1(R.drawable.floor_1, R.drawable.foggy_floor_1, R.drawable.beaten_floor_1),
    NormalBackground2(R.drawable.floor_2, R.drawable.foggy_floor_2, R.drawable.beaten_floor_2),
    NormalBackground3(R.drawable.floor_3, R.drawable.foggy_floor_3, R.drawable.beaten_floor_3);

    /**
     * Cell's background when cell is explored but not finished
     */
    private int normalBackground;

    /**
     * Cell's background when cell has not been visited yet
     */
    private int foggyBackground;

    /**
     * Cell's background when cell's Hostile is beaten
     */
    private int exploredBackground;

    /**
     * Default CellBackground constructor
     *
     * @param normalBackground:   Normal background
     * @param foggyBackground:    Foggy background
     * @param exploredBackground: Explored background
     */
    CellBackground(int normalBackground, int foggyBackground, int exploredBackground) {
        this.normalBackground = normalBackground;
        this.foggyBackground = foggyBackground;
        this.exploredBackground = exploredBackground;
    }

    /**
     * Get cell's normal background
     *
     * @return cell's normal background
     */
    public int getNormalBackground() {
        return normalBackground;
    }

    /**
     * Get cell's foggy background
     *
     * @return cell's foggy background
     */
    public int getFoggyBackground() {
        return foggyBackground;
    }

    /**
     * Get cell's explored background
     *
     * @return cell's explored background
     */
    public int getExploredBackground() {
        return exploredBackground;
    }

    /**
     * Assigning a background to the cell depending on it's position
     *
     * @param cellNumber:  Position of the button
     * @param dungeonSize: Dungeon size
     */
    public static CellBackground getBackground(int cellNumber, int dungeonSize) {
        int totalCellNumber = dungeonSize * dungeonSize;

        if (cellNumber == totalCellNumber - dungeonSize + 1) { // Bottom left corner
            return BottomLeftBackground;
        } else if (cellNumber == totalCellNumber) { // Bottom right corner
            return BottomRightBackground;
        } else if (cellNumber > (totalCellNumber - dungeonSize + 1) && cellNumber < totalCellNumber) { // Bottom
            return BottomBackground;
        } else if (Math.floorMod(cellNumber, dungeonSize) == 1) { // Left
            return LeftBackground;
        } else if (Math.floorMod(cellNumber, dungeonSize) == 0) { // Right
            return RightBackground;
        } else { // Setting a random normal floor
            int randomFloor = new Random().nextInt(4);
            switch (randomFloor) {
                case 0:
                    return NormalBackground;
                case 1:
                    return NormalBackground1;
                case 2:
                    return NormalBackground2;
                default:
                    return NormalBackground3;
            }
        }
    }
}
