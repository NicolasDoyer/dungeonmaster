package com.example.projetndoyer.entity.character;

import com.example.projetndoyer.R;

/**
 * Dark Angel class
 */
public class DarkAngel extends Hostile {

    /**
     * Default Dark Angel constructor
     *
     * @param power: Dar Angel's power
     */
    public DarkAngel(int power) {
        super(power);
        this.imageResource = R.drawable.dark_angel;
        this.name = "Dark Angel";
    }
}
