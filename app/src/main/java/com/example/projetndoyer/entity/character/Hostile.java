package com.example.projetndoyer.entity.character;

/**
 * Represents a Hostile
 */
public abstract class Hostile extends Character {

    /**
     * Hostile's image representation
     */
    int imageResource;

    /**
     * Hostile's name
     */
    String name;

    /**
     * Default Hostile constructor
     */
    public Hostile() {
        this.power = 1;
    }

    /**
     * Power Hostile constructor
     *
     * @param power: Hostile's power
     */
    public Hostile(int power) {
        this.power = power;
    }

    /**
     * Get Hostile image representation
     *
     * @return image resource
     */
    public int getImageResource() {
        return imageResource;
    }

    /**
     * Get Hostile's name
     *
     * @return Hostile's name
     */
    public String getName() {
        return name;
    }

    /**
     * Set Hostile'name
     *
     * @param name: Hostile's name
     */
    public void setName(String name) {
        this.name = name;
    }
}
