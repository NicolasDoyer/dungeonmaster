package com.example.projetndoyer.entity;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * LeaderBoard class
 */
public class GameLeaderboard implements Serializable {

    /**
     * Max score to display
     */
    private static final int MAX_SCORE = 10;

    /**
     * Score save filename
     */
    private static final String SAVE_FILENAME = "gamescores.sav";

    /**
     * Registered scores
     */
    private ArrayList<GameScore> gameScores;

    /**
     * Default LeaderboardActivity constructor
     *
     * @param context: context
     */
    public GameLeaderboard(Context context) {
        loadScores(context);
    }

    /**
     * Load scores from save file
     *
     * @param context: context
     */
    private void loadScores(Context context) {
        try {
            FileInputStream fis = context.openFileInput(SAVE_FILENAME);
            ObjectInputStream is = new ObjectInputStream(fis);
            this.gameScores = (ArrayList<GameScore>) is.readObject();
            is.close();
            fis.close();
        } catch (Exception e) {
            this.gameScores = new ArrayList<>(MAX_SCORE);
        }
        // If file empty generate new leaderboard
        if (gameScores == null) {
            this.gameScores = new ArrayList<>(MAX_SCORE);
        }
    }

    /**
     * Save scores to save file
     *
     * @param context: context
     */
    public void saveScores(Context context) {
        try {
            FileOutputStream fos = context.openFileOutput(SAVE_FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(gameScores);
            os.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add score to leaderboard
     *
     * @param gameScore: score to add
     */
    public void addScore(GameScore gameScore) {
        for (int i = 0; i < MAX_SCORE; i++) {
            if (i > gameScores.size() - 1) { // If there is a place then add the score
                gameScores.add(i, gameScore);
                break;
            } else {
                GameScore currentGameScore = gameScores.get(i);
                if (currentGameScore.getPlayerLevel() < gameScore.getPlayerLevel()) { // If new score level is better than the current one then add the new score
                    gameScores.add(i, gameScore);
                    break;
                } else if (currentGameScore.getPlayerLevel() == gameScore.getPlayerLevel()) { // If new score player's power is better than the current one then add the new score
                    if (currentGameScore.getPlayerPower() < gameScore.getPlayerPower()) {
                        gameScores.add(i, gameScore);
                        break;
                    }
                }
            }
        }
        if (gameScores.size() > MAX_SCORE) { // Delete all scores after MAX_SCORE index (keep only 10 scores)
            gameScores.subList(MAX_SCORE, gameScores.size()).clear();
        }
    }

    /**
     * Get LeaderBoard's game scores
     *
     * @return LeaderBoard's game scores
     */
    public ArrayList<GameScore> getGameScores() {
        return gameScores;
    }
}
