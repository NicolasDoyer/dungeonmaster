package com.example.projetndoyer.entity.character.helper;

import com.example.projetndoyer.entity.character.Hostile;

/**
 * Interface IHostileFactory
 */
public interface IHostileFactory {

    /**
     * Get an Hostile entity
     *
     * @param type:         Type of the hostile
     * @param minimalPower: Minimal hostile power
     * @param maximalPower: Maximal hostile power
     * @return Hostile
     */
    Hostile getHostile(int type, int minimalPower, int maximalPower);
}
