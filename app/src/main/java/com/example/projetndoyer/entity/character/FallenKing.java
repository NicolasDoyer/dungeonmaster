package com.example.projetndoyer.entity.character;

import com.example.projetndoyer.R;

/**
 * FallenKing class
 */
public class FallenKing extends Hostile {

    /**
     * Default Fallen King constructor
     *
     * @param power: Fallen King's power
     */
    public FallenKing(int power) {
        super(power);
        this.imageResource = R.drawable.fallen_king;
        this.name = "Fallen King";
    }
}
