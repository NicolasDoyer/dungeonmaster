package com.example.projetndoyer.entity.character;

import com.example.projetndoyer.R;

/**
 * Ectoplasm class
 */
public class Ectoplasm extends Hostile {

    /**
     * Default Ectoplasm constructor
     *
     * @param power: Ectoplasm's power
     */
    public Ectoplasm(int power) {
        super(power);
        this.imageResource = R.drawable.ectoplasm;
        this.name = "Ectoplasm";
    }
}
