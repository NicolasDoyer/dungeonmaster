package com.example.projetndoyer.entity;

import com.example.projetndoyer.entity.character.Player;
import com.example.projetndoyer.entity.dungeon.Dungeon;

import java.io.Serializable;

/**
 * Game class
 */
public class Game implements Serializable {

    private Player player;
    private Dungeon dungeon;
    private boolean finished = false;
    private boolean victory = false;
    private int currentLevel = 1;

    private static final int PLAYER_POWER_ON_WIN = 10;
    private static final int PLAYER_DAMAGE_ON_LOOSE = 3;
    private static final int PLAYER_DAMAGE_ON_ESCAPE = 1;

    /**
     * Game constructor
     */
    public Game() {
        player = new Player(GameSettings.initialPlayerPower, GameSettings.maxPlayerHP);
        dungeon = new Dungeon(GameSettings.initialMinHostilePower, GameSettings.initialMaxHostilePower, GameSettings.dungeonSize);
    }

    /**
     * Get player
     * @return get game player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Set player
     * @param player: set game player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * @return game dungeon
     */
    public Dungeon getDungeon() {
        return dungeon;
    }

    /**
     * Handle player win
     */
    public void onPlayerWin() {
        player.setPower(player.getPower() + PLAYER_POWER_ON_WIN);
        if(dungeon.getRemainingUnexploredCells() == 0) {
            finished = true;
            victory = true;
        }
    }

    /**
     * Handle player loose
     */
    public void onPlayerLoose() {
        player.setHp(player.getHp() - PLAYER_DAMAGE_ON_LOOSE);
        if(player.isDead()) {
            finished = true;
            victory = false;
        }
    }

    /**
     * Handle player escape
     */
    public void onPlayerEscape() {
        this.player.setHp(player.getHp() - PLAYER_DAMAGE_ON_ESCAPE);
        if(player.isDead()) {
            finished = true;
            victory = false;
        }
    }

    /**
     * Set game dungeon
     * @param dungeon: dungeon to set
     */
    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
    }

    /**
     * Check if game is finished
     * @return true if game is finished
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Set game status
     * @param finished: true if game is finished
     */
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    /**
     * Check if game is a victory
     * @return true if it's a victory
     */
    public boolean isVictory() {
        return victory;
    }

    /**
     * Set victory status
     * @param victory: status to set
     */
    public void setVictory(boolean victory) {
        this.victory = victory;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public void nextLevel() {
        currentLevel ++;
        player.setHp(player.getMaxHp());
        dungeon = new Dungeon(player.getPower() - 100, player.getPower() + 50, dungeon.getDungeonSize());
        victory = false;
        finished = false;
    }

}
