package com.example.projetndoyer.entity.item;

import android.graphics.Color;

import com.example.projetndoyer.R;
import com.example.projetndoyer.entity.character.Player;

import java.util.Random;

public class PowerCharm extends AbstractItem{

    private int additionalPower;

    public PowerCharm() {
        additionalPower = 5 + new Random().nextInt(6);
        imageResource = R.drawable.power_charm;
        description = "+ " + additionalPower + " POWER";
        descriptionColor = Color.GREEN;
    }

    @Override
    public void use(Player player) {
        player.setPower(player.getPower() + additionalPower);
    }
}
