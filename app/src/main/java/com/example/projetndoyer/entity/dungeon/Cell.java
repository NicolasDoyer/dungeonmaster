package com.example.projetndoyer.entity.dungeon;

import com.example.projetndoyer.entity.character.Hostile;
import com.example.projetndoyer.entity.character.helper.HostileFactory;
import com.example.projetndoyer.entity.item.AbstractItem;
import com.example.projetndoyer.entity.item.Usable;

import java.io.Serializable;

/**
 * Represents a dungeon's cell
 */
public class Cell implements Serializable {

    /**
     * Cell's Hostile
     */
    private Hostile hostile;

    /**
     * Cell's exploration status
     */
    private Boolean isExplored = false;

    /**
     * Possible cell's item
     */
    private AbstractItem item;

    /**
     * Default constructor
     *
     * @param minimalHostilePower: Minimal hostile's power
     * @param maximalHostilePower: Maximal hostile's power
     */
    public Cell(int minimalHostilePower, int maximalHostilePower) {
        HostileFactory hostileFactory = new HostileFactory();
        this.hostile = hostileFactory.getHostile(HostileFactory.RANDOM_POWER, minimalHostilePower, maximalHostilePower);
    }

    /**
     * Get cell hostile
     *
     * @return hostile in the cell
     */
    public Hostile getHostile() {
        return hostile;
    }

    /**
     * Set cell hostile
     *
     * @param hostile: hostile to set
     */
    public void setHostile(Hostile hostile) {
        this.hostile = hostile;
    }

    /**
     * Check of cell is already explored
     *
     * @return true is cell explored
     */
    public Boolean getExplored() {
        return isExplored;
    }

    /**
     * Set cell exploration
     *
     * @param explored: boolean to set
     */
    public void setExplored(Boolean explored) {
        isExplored = explored;
    }

    /**
     * Get possible cell's item
     *
     * @return cell's item
     */
    public AbstractItem getItem() {
        return item;
    }

    /**
     * Set cell's item
     *
     * @param item: item to set
     */
    public void setItem(AbstractItem item) {
        this.item = item;
    }
}
