package com.example.projetndoyer;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.projetndoyer.entity.GameLeaderboard;
import com.example.projetndoyer.entity.GameScore;

import java.text.DateFormat;
import java.util.Locale;

/**
 * LeaderBoard activity
 */
public class LeaderboardActivity extends AppCompatActivity {

    /**
     * Game leaderboard
     */
    private GameLeaderboard gameLeaderboard;

    /**
     * Score layout
     */
    private TableLayout scoreTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        gameLeaderboard = new GameLeaderboard(this);
        scoreTable = findViewById(R.id.leaderboard_table);
        initializeScores();
    }

    /**
     * Initialize score and add it to table layout
     */
    private void initializeScores() {

        for (GameScore gameScore : gameLeaderboard.getGameScores()) {
            // Used to convert score's date to readable format
            DateFormat shortDateFormatEN = DateFormat.getDateTimeInstance(
                    DateFormat.SHORT,
                    DateFormat.SHORT, new Locale("EN", "en"));

            TextView playerName = new TextView(this);
            playerName.setText(gameScore.getPlayerName());
            playerName.setTextColor(Color.WHITE);
            playerName.setGravity(Gravity.CENTER);

            TextView playerLevel = new TextView(this);
            playerLevel.setText(String.valueOf(gameScore.getPlayerLevel()));
            playerLevel.setTextColor(Color.WHITE);
            playerLevel.setGravity(Gravity.CENTER);

            TextView playerPower = new TextView(this);
            playerPower.setText(String.valueOf(gameScore.getPlayerPower()));
            playerPower.setTextColor(Color.WHITE);
            playerPower.setGravity(Gravity.CENTER);

            TextView date = new TextView(this);
            date.setText(shortDateFormatEN.format(gameScore.getDate()));
            date.setTextColor(Color.WHITE);
            date.setGravity(Gravity.CENTER);

            TableRow newRow = new TableRow(this);
            newRow.setBackgroundResource(R.color.colorPrimaryDark);
            newRow.addView(playerName);
            newRow.addView(playerLevel);
            newRow.addView(playerPower);
            newRow.addView(date);
            scoreTable.addView(newRow);
        }
    }
}
