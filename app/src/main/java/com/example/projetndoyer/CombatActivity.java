package com.example.projetndoyer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetndoyer.entity.Game;
import com.example.projetndoyer.entity.combat.Combat;
import com.example.projetndoyer.entity.dungeon.Cell;

/**
 * Combat activity
 */
public class CombatActivity extends AppCompatActivity {

    /**
     * Game extra id
     */
    public static final String GAME_EXTRA = "com.example.projetndoyer.game";

    /**
     * Cell extra id
     */
    public static final String CLICKED_CELL = "com.example.projetndoyer.cell";

    /**
     * Combat result id
     */
    public static final String COMBAT_RESULT = "com.example.projetndoyer.combat_result";

    /**
     * Un-serialized game
     */
    private Game game;

    /**
     * Un-serialized cell
     */
    private Cell cell;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combat);

        // Layout's fields
        TextView playerHP = findViewById(R.id.combat_player_hp);
        TextView playerPower = findViewById(R.id.combat_player_power);
        TextView hostilePower = findViewById(R.id.combat_hostile_power);
        TextView hostileLabel = findViewById(R.id.hostile_label);
        ImageView hostileImage = findViewById(R.id.hostile_image);
        TextView itemLabel = findViewById(R.id.item_label);
        ImageView itemImage = findViewById(R.id.item_image);
        TextView itemDescription = findViewById(R.id.item_description);

        // Recover intent
        Intent intent = getIntent();
        this.game = (Game) intent.getSerializableExtra(GAME_EXTRA);
        this.cell = (Cell) intent.getSerializableExtra(CLICKED_CELL);
        if (this.game != null) {
            playerHP.setText(String.valueOf(this.game.getPlayer().getHp()));
            playerPower.setText(String.valueOf(this.game.getPlayer().getPower()));
            hostilePower.setText(String.valueOf(this.cell.getHostile().getPower()));
            hostileLabel.setText(cell.getHostile().getName());
            hostileImage.setImageResource(cell.getHostile().getImageResource());
            if (cell.getItem() != null) {
                itemLabel.setText(R.string.bonus_item_label);
                itemDescription.setText(cell.getItem().getDescription());
                itemDescription.setTextColor(cell.getItem().getDescriptionColor());
                itemImage.setImageResource(cell.getItem().getImageResource());
            }
        }
    }

    /**
     * Handle attack
     *
     * @param view: current view
     */
    public void onAttack(View view) {
        boolean combatWon = Combat.launchCombat(this.game.getPlayer(), this.cell.getHostile());
        Intent intent = getIntent();
        if (combatWon) {
            intent.putExtra(COMBAT_RESULT, Combat.WIN_VALUE);
        } else {
            intent.putExtra(COMBAT_RESULT, Combat.LOSE_VALUE);
        }
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Handle escape
     *
     * @param view: current view
     */
    public void onEscape(View view) {
        Intent intent = getIntent();
        intent.putExtra(COMBAT_RESULT, Combat.ESCAPE_VALUE);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        onEscape(null);
    }
}
